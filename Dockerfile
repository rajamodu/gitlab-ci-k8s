FROM openjdk:11.0.3-slim as builder
WORKDIR /app
ADD . /app
RUN ./gradlew build

FROM openjdk:11.0.3-jre-slim
RUN apt-get update && apt-get install curl -y
WORKDIR /opt/app-root/src
COPY --from=builder /app/build/libs/gitlab-ci-k8s-1.0-SNAPSHOT.jar /opt/app-root/src/app.jar
CMD java -jar app.jar
