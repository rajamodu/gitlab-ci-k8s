package com.yanzord.gitlabcik8s.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class HelloServiceTest {

    @Autowired
    private HelloService helloService;

    @Test
    public void shouldReturnHelloTest() {
        String name = "Test";

        String expected = "Hello, Test";
        String actual = helloService.getHello(name);

        assertEquals(expected, actual);
    }
}
