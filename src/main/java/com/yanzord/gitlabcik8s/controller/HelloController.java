package com.yanzord.gitlabcik8s.controller;

import com.yanzord.gitlabcik8s.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @Autowired
    private HelloService helloService;

    @GetMapping(value = "/{name}")
    private ResponseEntity<String> getHello(@PathVariable("name") String name) {
        return new ResponseEntity<>(helloService.getHello(name), HttpStatus.OK);
    }
}
